import 'package:flutter/material.dart';
import 'package:user_hood/src/bloc/provider.dart';
import 'package:user_hood/src/preferences_user/preferences_user.dart';
import 'package:user_hood/src/routes/routes.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() async { 
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = new PreferenciasUsuario();
  await prefs.initPrefs();
  runApp(MyApp());
}
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // final prefs = new PreferenciasUsuario();
    return Provider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,        
        localizationsDelegates: [
          // ... app-specific localization delegate[s] here
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en'), // English
          const Locale('es'), // Hebrew
          const Locale.fromSubtags(languageCode: 'zh'), // Chinese *See Advanced Locales below*
          // ... other locales the app supports
        ],
        initialRoute:  '/',//(prefs.loggedIn) ? '/' : 'login',
        routes: getApplicationRoutes(),
        theme: ThemeData(
          primaryColor: Colors.deepOrange,
          primaryColorDark: Colors.deepOrange,
        ),
      ),
    );
  }
}