import 'package:flutter/material.dart';
import 'package:user_hood/src/utils/utils.dart';
class DataSearch extends SearchDelegate{
  
  
  String selection = '';
  
  final peliculas = [
    'Iron man',
    'Si tuviera 30',
    'BlackPanther',
    'Yoyo',
    'Ted',
    'Barbie'
  ];
  final peliculasrecientes = ['Spiderman','Batman'];
  @override
  List<Widget> buildActions(BuildContext context) {
      //acciones del appbar
      return [
        IconButton(
          icon: Icon(Icons.clear),
          onPressed: (){
            query = '';
          },
        ),
      ];
    }
  
    @override
    Widget buildLeading(BuildContext context) {
      return 
        IconButton(
          onPressed: (){
            close(
              context,
              null
            );
          },
          icon: AnimatedIcon(
            progress: transitionAnimation,
            icon: AnimatedIcons.menu_arrow,
          )
        );
    }
  
    @override
    Widget buildResults(BuildContext context) {
      return Center(
        child:Container(
          height: 100,
          color: Colors.blueAccent,
          child: Text(selection),
        )
      );
    }
  
    @override
    Widget buildSuggestions(BuildContext context) {
      return ListView(
        children: peliculas.map((movie){
          return Container(
            margin: EdgeInsets.symmetric(
              vertical: 10,
              horizontal: 10
            ),
            padding: EdgeInsets.symmetric(
              vertical: 10,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color:'#FFAA7A'.toColor(),
            ),
            child: ListTile(
              leading: 
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: FadeInImage(
                  image: NetworkImage('https://cdn01.enpelotas.com/cms/blog/images/thumbs/lg_f5a8b1393b806a.jpg'),
                  placeholder: AssetImage('assets/loading.gif'),
                  fit: BoxFit.contain,
                ),
              ),
              title: Text(movie, style: TextStyle(
                color: '#CA4C17'.toColor(),
              )),
              onTap: (){
                close(context, null);
                Navigator.pushNamed(context, 'detail', arguments: movie);
              },
            ),
          );
        }).toList()
      );
  }

}