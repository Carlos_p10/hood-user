import 'package:user_hood/src/models/menu_models.dart';
import 'package:user_hood/src/models/opciones.dart';

class FoodDetailArguments{
  FoodDetailArguments({
    this.menu,
    this.selectedOptDetail
  });

  final Menu menu;
  final List<Opciones> selectedOptDetail;
}