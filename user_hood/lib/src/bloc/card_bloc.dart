import 'package:rxdart/subjects.dart';
import 'package:user_hood/src/bloc/validators.dart';

class CardBloc with Validators{
    final _monthController  = BehaviorSubject<String>();
    final _yearController   = BehaviorSubject<String>();
    final _cvvController    = BehaviorSubject<String>();

    Stream<String> get monthStream  => _monthController.stream.transform(validMonthExpiration);
    Stream<String> get yearStream   => _yearController.stream.transform(validateYear);
    Stream<String> get cvvStream    => _cvvController.stream.transform(validateCVV);

    Function(String) get changeMonth  => _monthController.sink.add;
    Function(String) get changeYear   => _yearController.sink.add;
    Function(String) get changeCvv    => _cvvController.sink.add;

    String get month  => _monthController.value;
    String get year   => _yearController.value;
    String get cvv    => _cvvController.value;

    dispose(){
      _monthController?.close();
      _yearController?.close();
      _cvvController?.close();
    }   
}