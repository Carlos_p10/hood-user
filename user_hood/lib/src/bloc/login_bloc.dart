import 'dart:async';
import 'package:user_hood/src/bloc/validators.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc with Validators {

  final _nombreController    = BehaviorSubject<String>();
  final _userNameController    = BehaviorSubject<String>();
  final _direccionController    = BehaviorSubject<String>();
  final _emailController    = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();
  final _rpasswordController    = BehaviorSubject<String>();

  // Recuperar los datos del Stream
  Stream<String> get nombreStream    => _nombreController.stream;
  Stream<String> get userNameStream    => _userNameController.stream;
  Stream<String> get direccionStream    => _direccionController.stream;
  Stream<String> get emailStream    => _emailController.stream.transform( validarEmail );
  Stream<String> get passwordStream => _passwordController.stream.transform( validarPassword );
  Stream<String> get rpasswordStream => _rpasswordController.stream.transform( validarPassword )
  .doOnData((String c) { 
    if (0 != _passwordController.value.compareTo(c)) {
      _rpasswordController.addError('No son iguales.');//comprobar que las contrasenas sean iguales
    }
  });

  Stream<bool> get formValidStream2 => 
      CombineLatestStream.combine2(emailStream, passwordStream, (a, b) => true);

  Stream<bool> get formValidStream => 
      CombineLatestStream.combine3(emailStream, passwordStream, rpasswordStream, (a, b, c) => true);


  // Insertar valores al Stream
  Function(String) get changeNombre    => _nombreController.sink.add;
  Function(String) get changeUserName    => _userNameController.sink.add;
  Function(String) get changeDireccion    => _direccionController.add;
  Function(String) get changeEmail    => _emailController.sink.add;
  Function(String) get changePassword => _passwordController.sink.add;
  Function(String) get changeRPassword => _rpasswordController.sink.add;


  // Obtener el último valor ingresado a los streams
  String get nombre    => _nombreController.value;
  String get userName    => _userNameController.value;
  String get direccion    => _direccionController.value;
  String get email    => _emailController.value;
  String get password => _passwordController.value;
  String get rpassword => _rpasswordController.value;

  dispose() {
    _nombreController?.close();
    _userNameController?.close();
    _direccionController?.close();
    _emailController?.close();
    _passwordController?.close();
    _rpasswordController?.close();
  }

}

