import 'dart:async';
// import 'dart:convert';

import 'package:user_hood/src/bloc/provider.dart';

class Validators {
  LoginBloc bloc;
  //final _bloc = new LoginBloc();

  final validarEmail = StreamTransformer<String, String>.fromHandlers(
    handleData: (email, sink) {
    Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);

    if (regExp.hasMatch(email)) {
      sink.add(email);
    } else {
      sink.addError('Email no es correcto');
    }
  });

  final validarPassword = StreamTransformer<String, String>.fromHandlers(
    handleData: (password, sink) {
    if (password.length >= 6) {
      sink.add(password);
    } else {
      sink.addError('Más de 6 caracteres por favor');
    }
  });

  final validMonthExpiration = StreamTransformer<String, String>.fromHandlers(
    handleData: (month, sink){
      Pattern pattern = r'^[0-9][0-9]{1,1}$';
      RegExp regExp = new RegExp(pattern);
      
      if(regExp.hasMatch(month) && int.parse(month) <= 12){
        sink.add(month);
      }else{
        sink.addError('Mes invalido. ej:03 o 12');
      }
    }
  );

  final validateYear = StreamTransformer<String, String>.fromHandlers(
    handleData: (year, sink){
      var date = DateTime.now().toString();
      int parseDate = DateTime.parse(date).year;

      if(parseDate <= int.parse(year) && year.length == 4){
        sink.add(year);
      }
      else{
        sink.addError('Esta tarjeta ya esta expirada');
      }
    }
  );

  final validateCVV = StreamTransformer<String, String>.fromHandlers(
    handleData: (cvv, sink){
      if(cvv.length == 3){
        sink.add(cvv);
      }
      else{
        sink.addError('Codigo cvv invalido');
      }
    }
  );
}
