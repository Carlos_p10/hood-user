import 'dart:convert';

Facturas facturasFromJson(String str) => Facturas.fromJson(json.decode(str));

String facturasToJson(Facturas data) => json.encode(data.toJson());

class Facturas {
    Facturas({
        this.ok,
        this.msg,
        this.data,
    });

    bool ok;
    String msg;
    List<Factura> data;

    factory Facturas.fromJson(Map<String, dynamic> json) => Facturas(
        ok: json["ok"],
        msg: json["msg"],
        data: List<Factura>.from(json["data"].map((x) => Factura.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "ok": ok,
        "msg": msg,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class Factura {
    Factura({
        this.detalle,
        this.total,
        this.estado,
        this.status,
        this.id,
        this.usuario,
        this.createdAt,
        this.updatedAt,
        this.v,
    });

    List<dynamic> detalle;
    int total;
    String estado;
    bool status;
    String id;
    dynamic usuario;
    DateTime createdAt;
    DateTime updatedAt;
    int v;

    factory Factura.fromJson(Map<String, dynamic> json) => Factura(
        detalle: List<dynamic>.from(json["detalle"].map((x) => x)),
        total: json ["total"],
        estado: json["estado"],
        status: json["status"],
        id: json["_id"],
        usuario: json["usuario"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
    );

    Map<String, dynamic> toJson() => {
        "detalle": List<dynamic>.from(detalle.map((x) => x)),
        "total": total,
        "estado": estado,
        "status": status,
        "_id": id,
        "usuario": usuario,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
    };
}
