class Opciones {
  Opciones({
    this.id,
    this.opcion,
    this.precio,
    this.selected,
  });

  String idUnico;
  String id;
  String opcion;
  double precio;
  bool selected;

  factory Opciones.fromJson(Map<String, dynamic> json) => Opciones(
        id: json["_id"],
        opcion: json["opcion"],
        precio: json["precio"].toDouble(),
        selected: json["selected"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "opcion": opcion,
        "precio": precio,
        "selected": selected,
      };
}
