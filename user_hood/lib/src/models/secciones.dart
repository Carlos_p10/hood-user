import 'package:user_hood/src/models/opciones.dart';

class Secciones {
  Secciones(
      {this.limiteOpciones,
      this.opciones,
      this.id,
      this.nombre,
      this.multiple,
      this.counter,
      this.v,
      this.selectedOpt});
  int limiteOpciones;
  int counter;
  List<Opciones> opciones;
  List<Opciones> selectedOpt;
  String id;
  String nombre;
  bool multiple;
  int v;

  factory Secciones.fromJson(Map<String, dynamic> json) => Secciones(
      limiteOpciones: json["limiteOpciones"],
      counter: json['counter'],
      opciones: List<Opciones>.from(
          json["opciones"].map((x) => Opciones.fromJson(x))),
      id: json["_id"],
      nombre: json["nombre"],
      multiple: json["multiple"],
      v: json["__v"],
      selectedOpt: List());

  Map<String, dynamic> toJson() => {
        "limiteOpciones": limiteOpciones,
        "counter": counter,
        "opciones": List<dynamic>.from(opciones.map((x) => x.toJson())),
        "_id": id,
        "nombre": nombre,
        "multiple": multiple,
        "__v": v,
      };
}
