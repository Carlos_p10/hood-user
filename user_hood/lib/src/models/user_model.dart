
import 'dart:convert';

Users usersFromJson(String str) => Users.fromJson(json.decode(str));

String usersToJson(Users data) => json.encode(data.toJson());

class Users {
    Users({
        this.ok,
        this.msg,
        this.data,
    });

    bool ok;
    String msg;
    List<User> data;

    factory Users.fromJson(Map<String, dynamic> json) => Users(
        ok: json["ok"],
        msg: json["msg"],
        data: List<User>.from(json["data"].map((x) => User.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "ok": ok,
        "msg": msg,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class User {
    User({
        this.nombre,
        this.estado,
        this.popular,
        this.direccion,
        this.latitud,
        this.longitud,
        this.role,
        this.numVotes,
        this.totalScore,
        this.rating,
        this.id,
        this.usuario,
        this.correo,
        this.contrasenia,
        this.foto,
        this.createdAt,
        this.updatedAt,
        this.v,
    });

    String nombre;
    bool estado;
    bool popular;
    String direccion;
    String latitud;
    String longitud;
    String role;
    int numVotes;
    int totalScore;
    int rating;
    String id;
    String foto;
    String usuario;
    String correo;
    String contrasenia;
    DateTime createdAt;
    DateTime updatedAt;
    int v;

    factory User.fromJson(Map<String, dynamic> json) => User(
        nombre: json["nombre"],
        estado: json["estado"],
        popular: json["popular"],
        direccion: json["direccion"],
        latitud: json["latitud"],
        longitud: json["longitud"],
        role: json["role"],
        numVotes: json["num_votes"],
        totalScore: json["total_score"],
        rating: json["rating"],
        id: json["_id"],
        usuario: json["usuario"],
        correo: json["correo"],
        contrasenia: json["contrasenia"],
        foto: json["foto"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
    );

    Map<String, dynamic> toJson() => {
        "nombre": nombre,
        "estado": estado,
        "popular": popular,
        "direccion": direccion,
        "latitud": latitud,
        "longitud": longitud,
        "role": role,
        "num_votes": numVotes,
        "total_score": totalScore,
        "rating": rating,
        "_id": id,
        "usuario": usuario,
        "correo": correo,
        "contrasenia": contrasenia,
        "foto": foto,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
    };
}
