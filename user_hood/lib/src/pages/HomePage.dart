import 'package:flutter/material.dart';
import 'package:user_hood/src/models/user_model.dart';
import 'package:user_hood/src/providers/usuario_provider.dart';
import 'package:user_hood/src/utils/utils.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[_header(context), _listRestaurant(context)],
        ),
      ),
    );
  }

  Widget _header(BuildContext context) {
    final container = Container(
      width: double.infinity,
      height: 250,
      color: '#FFAA7A'.toColor(),
    );

    final columnHood = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('HOOD',
            style: TextStyle(
                color: '#CA4C17'.toColor(),
                fontSize: 40,
                fontWeight: FontWeight.w700)),
        Text('Bienvenido',
            style: TextStyle(color: '#CA4C17'.toColor(), fontSize: 25)),
      ],
    );

    final carContainer = GestureDetector(
      onTap: () => Navigator.pushNamed(context, 'cart'),
      child: Container(
        alignment: Alignment.topCenter,
        child: Icon(Icons.shopping_cart,
          color: '#CA4C17'.toColor(),      
        ),
      ),
    );

    //text hood and welcome
    final textColumn = SafeArea(
        child: Container(
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                columnHood,
                carContainer
              ],
            )));

    //widget to get the other widgets and put the cards over the background
    return Stack(
      children: <Widget>[container, textColumn, _crearSliderCombo()],
    );
  }

  Widget _crearSliderCombo() {
    final userProvider = UsuarioProvider();
    return FutureBuilder(
      future: userProvider.getUserEnterprise(),
      builder: (context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          return _sliderRestaurants(snapshot.data);
        } else {
          return Container(child: Image.asset('assets/loading.gif'));
        }
      },
    );
  }

  Widget _sliderRestaurants(List<User> users) {
    return Container(
      margin: EdgeInsets.only(top: 180, bottom: 10),
      height: 220,
      width: double.infinity,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: users.length,
        itemBuilder: (context, i) => _restHCard(context, users[i]),
      ),
    );
  }

  Widget _restHCard(BuildContext context, User user) {
    //de aca solo he cambiado los TEXT
    final restH = Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      width: 300,
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10)),
              child: FadeInImage(
                fit: BoxFit.cover,
                width: double.infinity,
                height: 100,
                placeholder: AssetImage('assets/loading.gif'),
                image: NetworkImage('https://i.imgur.com/9ibjeXO.jpg'),
                fadeInDuration: Duration(milliseconds: 200),
              ),
            ),
            Row(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          Container(
                            alignment: Alignment.topLeft,
                            child: Text(user.nombre, //'Burguer King', //titulo
                                style: TextStyle(fontWeight: FontWeight.w900)),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 170),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Icon(Icons.timer, size: 17),
                                SizedBox(width: 5),
                                Text('30 - 40 min'), //tiempo
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: <Widget>[
                          Icon(Icons.location_on, size: 15),
                          SizedBox(width: 5),
                          Text(user.direccion), //'San salvador'), //Ubicacion
                        ],
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: <Widget>[
                          Icon(Icons.star, size: 15, color: Colors.yellow),
                          Icon(Icons.star, size: 15, color: Colors.yellow),
                          Icon(Icons.star, size: 15, color: Colors.yellow),
                          Icon(Icons.star, size: 15, color: Colors.yellow),
                          Icon(Icons.star, size: 15, color: Colors.yellow),
                          SizedBox(width: 10),
                          Text(user.rating.toString(),
                              style: TextStyle(fontSize: 12)) //rating
                        ],
                      ),
                      SizedBox(height: 5),
                      Container(
                        margin: EdgeInsets.only(top: 5),
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                        child: Text('Nuevo',
                            style:
                                TextStyle(fontSize: 13, color: Colors.green)),
                        decoration: BoxDecoration(
                            color: Colors.green[100],
                            borderRadius: BorderRadius.circular(10)),
                      )
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );

    return GestureDetector(
      child: restH,
      onTap: () => Navigator.pushNamed(context, 'restaurant', arguments: user),
    );
  }

  Widget _listRestaurant(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: '#FFAA7A'.toColor(), borderRadius: BorderRadius.circular(10)),
      margin: EdgeInsets.symmetric(horizontal: 10),
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Text('Explorar',
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w900,
                        color: '#CA4C17'.toColor())),
              ),
              Container(
                child: GestureDetector(
                  onTap: () => Navigator.pushNamed(context, 'restaurants'),
                  child: Text('Ver todos',
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w700,
                          color: '#CA4C17'.toColor())),
                ),
              ),
            ],
          ),
          _crearListRestaurants() 
          // _restaurantCard(),
        ],
      ),
    );
  }

  Widget _crearListRestaurants() {
    final userProvider = UsuarioProvider();
    return FutureBuilder(
      future: userProvider.getUserEnterprise(),
      builder: (context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          return _listRestaurants(snapshot.data);
        } else {
          return Container(child: Image.asset('assets/loading.gif'));
        }
      },
    );
  }

  Widget _listRestaurants(List<User> users) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      width: double.infinity,
      height: 350,
      child: ListView.builder(
        // scrollDirection: Axis.vertical,
        // shrinkWrap: true,
        itemCount: users.length,
        itemBuilder: (context, i) => _restaurantCard(context, users[i]),
      ),
    );
  }

  Widget _restaurantCard(BuildContext context, User user,
      {name, image, location, time}) {
    final restV = Column(
      children: <Widget>[
        Container(
          // height: 120,
          margin: EdgeInsets.symmetric(vertical: 10),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: <Widget>[
              Container(
                  padding: EdgeInsets.all(10),
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(10)),
                  width: 150,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: FadeInImage(
                      fit: BoxFit.cover,
                      placeholder: AssetImage('assets/loading.gif'),
                      image: NetworkImage(
                          'https://www.ocregister.com/wp-content/uploads/2019/09/OCR-L-TACOBELL-0906-1.jpg'),
                    ),
                  )),
              Container(
                padding: EdgeInsets.symmetric(
                  vertical: 20,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(user.nombre, //titulo
                        style: TextStyle(fontWeight: FontWeight.w800)),
                    SizedBox(height: 5),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.location_on,
                          size: 15,
                        ),
                        SizedBox(width: 5),
                        Text(user.direccion) //lugar
                      ],
                    ),
                    SizedBox(height: 5),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.timer,
                          size: 15,
                        ),
                        SizedBox(width: 5),
                        Text('30 - 40 min.') //tiempo
                      ],
                    ),
                    SizedBox(height: 3),
                    Row(
                      children: <Widget>[
                        Icon(Icons.star, size: 15, color: Colors.yellow),
                        Icon(Icons.star, size: 15, color: Colors.yellow),
                        Icon(Icons.star, size: 15, color: Colors.yellow),
                        Icon(Icons.star, size: 15, color: Colors.yellow),
                        Icon(Icons.star, size: 15, color: Colors.yellow),
                        SizedBox(width: 10),
                        Text(user.rating.toString(),
                            style: TextStyle(fontSize: 12)) //rating
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        )
      ],
    );

    return GestureDetector(
      child: restV,
      onTap: () => Navigator.pushNamed(context, 'restaurant', arguments: user),
    );
  }
}
