import 'package:flutter/material.dart';
import 'package:user_hood/src/utils/utils.dart';
class ProfilePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:'#FFAA7A'.toColor(),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              _headerProfile(),
              SizedBox(height: 20),
              _menuProfile(context)
            ],
          ),
        ),
      ),
    );
  }

  Widget _headerProfile() {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 50
      ),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(100),

        )
      ),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: FadeInImage(
                  height: 100,
                  width: 100,
                  placeholder: AssetImage('assets/loading.gif'),
                  image: NetworkImage('https://rmi.org/wp-content/uploads/2017/04/RobertoZanchi_square-500x500.jpg'),
                ),
              )
            ],
          ),
          SizedBox(height: 20),
          Text('Alejandro Gonzalez', style: TextStyle(
            fontSize: 20
          )),
          Text('San Salvador, El Salvador')
        ],
      ),
    );
  }

  Widget _menuProfile(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 20
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10)
      ),
      child: Column(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.pin_drop),
            title: Text('Mis direcciones'),
            onTap: () => Navigator.pushNamed(context, 'proxim'),
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.credit_card),
            title: Text('Mis tarjetas'),
            onTap: (){
              Navigator.pushNamed(context, 'mycards');
            },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.confirmation_number),
            title: Text('Mis cupones'),
            onTap: () => Navigator.pushNamed(context, 'proxim'),
          ),
           ListTile(
              leading: Icon(Icons.settings_power),
              title: Text('Cerrar sesion'),
              onTap: () => Navigator.pushNamed(context, 'login'),
            ),
          
        ],
      ),
    );
  }
}