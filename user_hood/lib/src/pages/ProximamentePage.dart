import 'package:flutter/material.dart';
import 'package:user_hood/src/utils/utils.dart';

class Proximamente extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final style = TextStyle(
      fontSize: 25
    );
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () => Navigator.pop(context),
          child: Container(
            alignment: Alignment.topLeft,
            child: Icon(Icons.arrow_back, size: 35, color: '#CA4C17'.toColor()),
          ),
        ),
      ),
      body: Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Center(
            child: Text("Proximamente", style: style,),
          )),
    );
  }
}
