import 'package:flutter/material.dart';
import 'package:user_hood/src/models/menu_models.dart';
import 'package:user_hood/src/models/user_model.dart';
import 'package:user_hood/src/providers/menu_provider.dart';
import 'package:user_hood/src/utils/utils.dart';

class RestaurantPage extends StatelessWidget {
  final _style = TextStyle(color: Colors.grey[600]);
  @override
  Widget build(BuildContext context) {
    final User user = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      // backgroundColor: '#ffffff'.toColor(),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _header(context, user),
              _crearCombo(user),
              // _combos(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _header(BuildContext context, User user) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        children: <Widget>[
          GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Container(
              alignment: Alignment.topLeft,
              child:
                  Icon(Icons.arrow_back, size: 35, color: '#CA4C17'.toColor()),
            ),
          ),
          SizedBox(height: 10),
          Row(
            children: <Widget>[
              Container(
                height: 130,
                child: Row(
                  children: <Widget>[
                    Container(
                        alignment: Alignment.topCenter,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10)),
                        width: 150,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: FadeInImage(
                            fit: BoxFit.cover,
                            placeholder: AssetImage('assets/loading.gif'),
                            image: NetworkImage(
                                'https://www.ocregister.com/wp-content/uploads/2019/09/OCR-L-TACOBELL-0906-1.jpg'),
                          ),
                        )),
                    Container(
                      padding: EdgeInsets.symmetric(
                        vertical: 20,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(user.nombre,
                              style: TextStyle(
                                fontWeight: FontWeight.w800,
                              )),
                          SizedBox(height: 5),
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.location_on,
                                color: Colors.grey[600],
                                size: 15,
                              ),
                              SizedBox(width: 5),
                              Text(
                                user.direccion,
                                style: _style,
                              )
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.timer,
                                color: Colors.grey[600],
                                size: 15,
                              ),
                              SizedBox(width: 5),
                              Text(
                                '30 - 40 min.',
                                style: _style,
                              )
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _crearCombo(User user) {
    final menuProvider = MenuProvider();
    // print(menuProvider.getMenuxUser(user.id));
    print(user.id);
    return FutureBuilder(
      future: menuProvider.getMenuxUser(user.id),
      builder: (context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          return _combos(context, snapshot.data);
        } else {
          return Container(child: Image.asset('assets/loading.gif'));
        }
      },
    );
  }

  Widget _combos(BuildContext context, List<Menu> menus) {
    return Container(
        width: double.infinity,
        height: 700, 
        //la altura tiene algo que ver antes esto no estaba    -----------------
        // color:'#FFAA7A'.toColor(),
        decoration: BoxDecoration(
            color: '#FFAA7A'.toColor(),
            borderRadius: BorderRadius.circular(10)),
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: ListView.builder(
            // shrinkWrap: true, lo que me permitia ver los elementos sin el height era esta parte -------------
            // scrollDirection: Axis.vertical,
            itemCount: menus.length,
            itemBuilder: (context, i) => _combosCard(context, menus[i])));
  }

  Widget _combosCard(BuildContext context, Menu menu) {
    return GestureDetector(
        onTap: () => Navigator.pushNamed(context, 'detalleComida', arguments: menu),
        child: Container(
            // height: ,
            margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(10)),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10)),
                        width: 120,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: FadeInImage(
                            fit: BoxFit.cover,
                            placeholder: AssetImage('assets/loading.gif'),
                            image: NetworkImage(
                                'https://revistamqe.com/wp-content/uploads/2016/05/MontajeTacoSupreme.png'),
                          ),
                        )),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(menu.nombre,
                              maxLines: 3, //'Taco al Pastor',
                              style: TextStyle(
                                  fontWeight: FontWeight.w800)), //Titulo
                          SizedBox(height: 5),
                          Row(
                            children: <Widget>[
                              Text('\$'), //4.50'),//Precio
                              Text(menu.precio.toString())
                            ],
                          ),

                          SizedBox(height: 5),
                          Row(
                            children: <Widget>[
                              Icon(Icons.star, size: 15, color: Colors.yellow),
                              Icon(Icons.star, size: 15, color: Colors.yellow),
                              Icon(Icons.star, size: 15, color: Colors.yellow),
                              Icon(Icons.star, size: 15, color: Colors.yellow),
                              Icon(Icons.star, size: 15, color: Colors.yellow),
                              SizedBox(width: 10),
                              Text(menu.rating.toString(),
                                  style: TextStyle(fontSize: 12)) //Score
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  child: Text(
                    menu.descripcion,
                    textAlign: TextAlign.start,
                  ), //descripcion
                )
              ],
            )));
  }
}
