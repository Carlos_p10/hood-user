import 'package:flutter/material.dart';
import 'package:user_hood/search/search_delegate.dart';
import 'package:user_hood/src/models/user_model.dart';
import 'package:user_hood/src/providers/usuario_provider.dart';
import 'package:user_hood/src/utils/utils.dart';

class RestaurantsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: '#FFAA7A'.toColor(),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                _header(context),
                _searchInput(context),
                _listRestaurant(context)
              ],
            ),
          ),
        ));
  }

  Widget _header(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              GestureDetector(
                onTap: () => Navigator.pop(context),
                child: Icon(Icons.arrow_back,
                    size: 35, color: '#CA4C17'.toColor()),
              )
            ],
          ),
          SizedBox(height: 10),
          Row(
            children: <Widget>[
              Text('Restaurantes',
                  style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.w700,
                      color: '#CA4C17'.toColor()))
            ],
          )
        ],
      ),
    );
  }

  Widget _searchInput(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        onTap: () {
          showSearch(context: context, delegate: DataSearch());
        },
        decoration: InputDecoration(
          hintText: 'Busca un restaurante...',
          prefixIcon: Icon(Icons.search),
          filled: true,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide.none,
          ),
          fillColor: Colors.white,
        ),
      ),
    );
  }

  Widget _listRestaurant(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: '#FFAA7A'.toColor(), borderRadius: BorderRadius.circular(10)),
      margin: EdgeInsets.symmetric(horizontal: 10),
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: Column(
        children: <Widget>[
          _crearListRestaurants()
          // _restaurantCard(context),
          // _restaurantCard(context),
        ],
      ),
    );
  }

  Widget _crearListRestaurants() {
    final menuProvider = UsuarioProvider();
    return FutureBuilder(
      future: menuProvider.getUserEnterprise(),
      builder: (context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          return _listRestaurants(snapshot.data);
        } else {
          return Container(child: Image.asset('assets/loading.gif'));
        }
      },
    );
  }

  Widget _listRestaurants(List<User> users) {
    return Container(
      margin: EdgeInsets.only(top: 10, bottom: 10),
      width: double.infinity,
      height: 700,
      child: ListView.builder(
        // scrollDirection: Axis.vertical,
        // shrinkWrap: true,
        itemCount: users.length,
        itemBuilder: (context, i) => _restaurantCard(context, users[i]),
      ),
    );
  }

  Widget _restaurantCard(BuildContext context, User user,
      {name, image, location, time}) {
    final rest = Column(
      children: <Widget>[
        Container(
          height: 120,
          margin: EdgeInsets.symmetric(vertical: 10),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: <Widget>[
              Container(
                  padding: EdgeInsets.all(10),
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(10)),
                  width: 150,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: FadeInImage(
                      fit: BoxFit.cover,
                      placeholder: AssetImage('assets/loading.gif'),
                      image: NetworkImage(
                          'https://www.ocregister.com/wp-content/uploads/2019/09/OCR-L-TACOBELL-0906-1.jpg'),
                    ),
                  )),
              Container(
                padding: EdgeInsets.symmetric(
                  vertical: 20,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(user.nombre, //nombre
                        style: TextStyle(fontWeight: FontWeight.w800)),
                    SizedBox(height: 5),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.location_on,
                          size: 15,
                        ),
                        SizedBox(width: 5),
                        Text(user.direccion) //lugar
                      ],
                    ),
                    SizedBox(height: 5),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.timer,
                          size: 15,
                        ),
                        SizedBox(width: 5),
                        Text('30 - 40 min.') //tiempo
                      ],
                    ),
                    SizedBox(height: 3),
                    Row(
                      children: <Widget>[
                        Icon(Icons.star, size: 15, color: Colors.yellow),
                        Icon(Icons.star, size: 15, color: Colors.yellow),
                        Icon(Icons.star, size: 15, color: Colors.yellow),
                        Icon(Icons.star, size: 15, color: Colors.yellow),
                        Icon(Icons.star, size: 15, color: Colors.yellow),
                        SizedBox(width: 10),
                        Text(user.rating.toString(),
                            style: TextStyle(fontSize: 12)) //rating
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        )
      ],
    );

    return GestureDetector(
      child: rest,
      onTap: () => Navigator.pushNamed(context, 'restaurant', arguments: user),
    );
  }
}
