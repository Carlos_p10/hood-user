import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:user_hood/src/bloc/card_bloc.dart';
import 'package:user_hood/src/utils/utils.dart';
import '../utils/utils.dart';
class AddCard extends StatefulWidget {
  @override
  _AddCardState createState() => _AddCardState();
}

class _AddCardState extends State<AddCard> {

  String numberCard='';
  final numberCardController = TextEditingController();
  String nameOfCard='';
  String expirationDate='';
  String securityNumber;
  CardBloc cardBloc = new CardBloc();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 20,
          vertical: 20
        ),
        child: SingleChildScrollView(
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Añadir tarjeta', style: TextStyle(
                  fontSize: 30
                )),
                SizedBox(height: 20),
                cardExample(),
                SizedBox(height: 20),
                numberCardInput(),
                SizedBox(height: 10),
                nameCardInput(),
                SizedBox(height: 20),
                detailsCard()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget cardExample(){
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(
        horizontal:20,
        vertical: 30
      ),
      decoration: BoxDecoration(
        color: '#CA4C17'.toColor(),
        borderRadius: BorderRadius.circular(10)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(getCardTypeFrmNumber(numberCard), style: TextStyle(
            color: Colors.white
          )),
          SizedBox(height: 30),
          Text('$numberCard', style: TextStyle(
            fontSize: 25,
            color: Colors.white
          )),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Card holder', style: TextStyle(
                color: Colors.white
              )),
              Text('Ex Date', style: TextStyle(
                color: Colors.white
              )),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('$nameOfCard', style: TextStyle(
                color: Colors.white
              )),
              Text('$expirationDate', style: TextStyle(
                color: Colors.white
              ))
            ],
          )
        ],
      ),
    );
  }

  Widget numberCardInput(){
    return TextField(
      maxLength: 16,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        hintText: 'Numero de la tarjeta',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
      ),
      onChanged: (value){
        setState(() {
          if(value.length > 16){}
          else{
            numberCard = StringUtils.addCharAtPosition(value, "-", 4, repeat: true);
          }
        });
      },
    );
  }
  Widget nameCardInput(){
    return TextField(
      decoration: InputDecoration(
        hintText: 'Titular de la tarjeta',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
      ),
      onChanged: (value){
        setState(() {
          nameOfCard = value;
        });
      },
    );
  }
  
  Widget detailsCard(){
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          monthInput(cardBloc),
          yearInput(cardBloc),
          securityCode(cardBloc)
        ],
    );
  }

  Widget monthInput(CardBloc bloc){
    return Flexible(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.30,
        child: StreamBuilder(
        stream: bloc.monthStream,
          builder: (BuildContext context, AsyncSnapshot snapshot){
            return TextField(
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              errorText: snapshot.error,
              hintText: 'Mes de vencimiento',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
            ),
            onChanged: (value)=>bloc.changeMonth(value)
            );
          },
        )
      ),
    );
  }
  Widget yearInput(CardBloc bloc){
    return Flexible(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.30,
        child: StreamBuilder(
        stream: bloc.yearStream,
        builder: (BuildContext context, AsyncSnapshot snapshot){
            return TextField(
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              errorText: snapshot.error,
              hintText: 'Año de vencimiento',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
            ),
            onChanged: (value)=>bloc.changeYear(value)
            );
          }
        ),
      ),
    );
  }
  Widget securityCode(CardBloc bloc){
    return Flexible(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.25,
        child: StreamBuilder(
          stream: bloc.cvvStream,
          builder: (BuildContext context, AsyncSnapshot snapshot){
            return TextField(
              decoration: InputDecoration(
                hintText: 'cvv',
                errorText: snapshot.error,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
              ),
              onChanged: (value)=>bloc.changeCvv(value)
            );
          }
        )
      ),
    );
  }
}