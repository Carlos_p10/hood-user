import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:user_hood/src/arguments/FoodDetailArguments.dart';
import 'package:user_hood/src/models/menu_models.dart';
import 'package:user_hood/src/models/opciones.dart';
import 'package:user_hood/src/models/secciones.dart';
import 'package:user_hood/src/utils/utils.dart';

class DetalleComida extends StatefulWidget {
  @override
  _DetalleComidaState createState() => _DetalleComidaState();
}

class _DetalleComidaState extends State<DetalleComida> {
  final _style = TextStyle(color: Colors.white);

  final _style2 = GoogleFonts.inter(
    color: Colors.white,
    fontSize: 18,
    fontWeight: FontWeight.w400,
  );

  List<Opciones> selectedOptDetail;

  double itemsPrice = 0.0;
  @override
  Widget build(BuildContext context) {
    final Menu menu = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      bottomNavigationBar: _buttonCart(context, menu),
      backgroundColor: '#FFAA7A'.toColor(),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.symmetric(
              vertical: 20,
            ),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      alignment: Alignment.topLeft,
                      child: Icon(Icons.arrow_back,
                          size: 35, color: '#CA4C17'.toColor()),
                    ),
                  ),
                ),
                SizedBox(height: 30),
                _product(context, menu),
                _mainSection(menu),
                _counterContainer()
                // _submenu(),
              ],
            )),
      )),
    );
  }

  Widget _product(BuildContext context, Menu menu) {
    final precio = ((menu.precio + itemsPrice) * _conteo).toString();
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(10)),
                  width: 120,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: FadeInImage(
                      fit: BoxFit.cover,
                      placeholder: AssetImage('assets/loading.gif'),
                      image: NetworkImage(
                          'https://revistamqe.com/wp-content/uploads/2016/05/MontajeTacoSupreme.png'),
                    ),
                  )),
              Container(
                padding: EdgeInsets.symmetric(vertical: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(menu.nombre, //Titulo menu
                        style: TextStyle(
                            fontWeight: FontWeight.w800,
                            color: Colors.white,
                            fontSize: 20)),
                    SizedBox(height: 5),
                    Text(
                      '\$ $precio', //precio
                      style: _style,
                    ),
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: 20),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Text(
              menu.descripcion, //descripcion
              style: _style,
              textAlign: TextAlign.justify,
            ),
          )
        ],
      ),
    );
  }

  Widget _mainSection(Menu menu) {
    return Container(
        width: double.infinity,
        margin: EdgeInsets.symmetric(vertical: 2),
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        height: 450,
        decoration: BoxDecoration(
            color: '#FFAA7A'.toColor(),
            borderRadius: BorderRadius.circular(10)),
        child: ListView.builder(
            shrinkWrap: true,
            itemCount: menu.secciones.length,
            itemBuilder: (context, i) => _mainSectionCheck(menu.secciones[i])));
  }

  Widget _mainSectionCheck(Secciones section) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      // height: 200,
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 30),
            child: Container(
              alignment: Alignment.topLeft,
              child: Text(
                section.nombre,
                style: TextStyle(
                    color: '#CA4C17'.toColor(),
                    fontSize: 20,
                    fontWeight: FontWeight.w800),
              ),
            ),
          ),
          _checkBox(section),
        ],
      ),
    );
  }

  Widget _checkBox(Secciones section) {
    //limit options from API
    //var limit = section.limiteOpciones;
    return Container(
        width: double.infinity,
        margin: EdgeInsets.symmetric(vertical: 2),
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
        // height: 150,
        decoration: BoxDecoration(
            color: '#FFAA7A'.toColor(),
            borderRadius: BorderRadius.circular(10)),
        child: ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: section.opciones.length,
            itemBuilder: (context, i) {
              return _optionRadio(section, i);
            }));
  }

  String isZero(double n) => n == 0.0 ? '0' : n.toString();

  // Widget _optionCheck(Opciones ingrediente) {
  //   final price = isZero(ingrediente.precio);
  //   return Container(
  //     margin: EdgeInsets.all(10),
  //     decoration: BoxDecoration(
  //       // color: Colors.white,
  //       border: Border.all(color: Color(0xffffffff)),
  //       borderRadius: BorderRadius.circular(5.0),
  //     ),
  //     child: Row(
  //       mainAxisAlignment: MainAxisAlignment.spaceAround,
  //       children: <Widget>[
  //         Checkbox(
  //           value: ingrediente.selected,
  //           // value: false,
  //           onChanged: (val) {
  //             setState(() {
  //               ingrediente.selected = !ingrediente.selected;
  //               print('${ingrediente.opcion} ${ingrediente.selected} ');
  //             });
  //           },
  //         ),
  //         Text(ingrediente.opcion, style: _style),
  //         // Text('hey', style: _style),
  //         Text('\$ $price', style: _style)
  //       ],
  //     ),
  //   );
  // }

  Widget _optionRadio(Secciones section, int i) {
    final option = section.opciones[i];
    final price = isZero(option.precio);
    option.idUnico = UniqueKey().toString();
    return Container(
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xffffffff)),
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Checkbox(
            value: option.selected,
            onChanged: (val) {              
              (section.selectedOpt.length == section.limiteOpciones && val)
                  ? print(
                      '${section.nombre} maximo alcanzado ${section.limiteOpciones}')
                  : setState(() {
                      //change selected option
                      option.selected = val;
                      val
                          ? section.selectedOpt.add(option)
                          : section.selectedOpt.remove(option);
                    });
              // selectedOptDetail = section.selectedOpt;
              print(section.selectedOpt.last.opcion);

              print(section.selectedOpt.length);
              // print(selectedOptDetail.last.opcion);
              // print(selectedOptDetail.length);
            },
          ),
          Text(option.opcion, style: _style),
          Text('\$ $price', style: _style)
        ],
      ),
    );    
  }

  void showAlert(int i) {
    AlertDialog(
      title: Text('Maximos seleccionables $i opciones'),
      actions: [
        FlatButton(
            onPressed: () => Navigator.of(context).pop(),
            child: Text('Aceptar'))
      ],
    );
  }

  int _conteo = 1;
  Widget _counterContainer() {
    return Column(
      children: <Widget>[
        Text("Cantidad"),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _circleContainer("-"),
            SizedBox(width: 5),
            Text("$_conteo"),
            SizedBox(width: 5),
            _circleContainer("+"),
          ],
        )
      ],
    );
  }

  Widget _circleContainer(String txt) {
    return GestureDetector(
      onTap: () => txt == "+" ? _add() : _min(),
      child: Container(
        alignment: Alignment.center,
        width: 20,
        height: 20,
        decoration: BoxDecoration(
            border: Border.all(color: Color(0xfffcb606)),
            color: Colors.white,
            shape: BoxShape.circle),
        child: Text(txt),
      ),
    );
  }

  void _add() => setState(() => _conteo++);
  void _min() => setState(() => _conteo--);

  // void _validateCounter() =>
  //     _conteo < 0 || _conteo > 25 ? _conteo = 0 : _conteo = _conteo;

  Widget _buttonCart(BuildContext context, Menu menu) {
    final bottomName = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          child: Text(
            "Agregar a la orden",
            style: _style2,
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(width: 10),
        Icon(FontAwesomeIcons.arrowRight, color: Colors.white, size: 20),
      ],
    );

    return Container(
      child: GestureDetector(
        onTap: () => Navigator.pushNamed(context, 'cart', arguments: menu),
        child: Container(
            height: 60,
            margin: EdgeInsets.only(bottom: 20),
            padding: EdgeInsets.all(5),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Color(0xffCA4C17),
                border: Border.all(color: Color(0xffCA4C17)),
                borderRadius: BorderRadius.circular(5.0)),
            child: bottomName),
      ),
    );
  }
}
