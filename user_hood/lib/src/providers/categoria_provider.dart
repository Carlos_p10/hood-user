import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:user_hood/src/models/categoria_models.dart';


class CategoryProvider{
  // Response resp;
  final dio = new Dio();
  final String _url = 'hood-delivery.herokuapp.com';

  final opts= Options(followRedirects: false, 
    validateStatus: (status){
    return status < 500;
  });
  
  Future<List<Categoria>> _procesarRespuesta(Uri url) async { 
    // final resp = await dio.get(url, options: opts);//si no funciona el String url y el dio cambiarlo por el http
    // final decodedData = resp.data;
    // final categorias = new Categorias.fromJson(decodedData['data']);
    final resp = await http.get(url);
    final decodedData = json.decode(resp.body);
    final categorias = new Categorias.fromJson(decodedData);
    print(categorias.status);
    print(categorias.data[0].nombre);
    return categorias.data;
  }

  Future<List<Categoria>> getCategory() async {    
    final url = Uri.https(_url, 'ws/categoria/listar');    
    return await _procesarRespuesta(url);
  }

  Future<List<Categoria>> getCategoryDetalle(String categoryId) async { 
    final url = Uri.https(_url, '$categoryId'); //'$_url/$categoryId';
    return await _procesarRespuesta(url);
  }
  
  

}