import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:user_hood/src/models/Factura_model.dart';

class FacturaProvider {
  Response resp;
  final dio = new Dio();
  final String _url = 'https://dev-hood.herokuapp.com/ws/bills';

  final opts = Options(
      followRedirects: false,
      validateStatus: (status) {
        return status < 500;
      });

  Future<List<Factura>> _procesarRespuesta(String url) async {
    final resp = await dio.get(url, options: opts);
    final decodedData = resp.data;
    final facturas = new Facturas.fromJson(decodedData);
    // print(facturas.msg);
    // print(facturas.data[0].total);
    return facturas.data;
  }

  Future<List<Factura>> getFactura() async {
    final String url = '$_url/all';
    return await _procesarRespuesta(url);
  }

  Future<Map<String, dynamic>> postNewFactura(
      String userCode, String detailCode, String tot) async {
    final String url = '$_url/new';
    final newFactura = {
      'usuario': userCode,
      'detalle': detailCode,
      'total': tot      
    };
    resp = await dio.post(url, data: json.encode(newFactura), options: opts);

    Map<String, dynamic> decodedResp = resp.data;
    return {'ok': true, 'token': decodedResp['msg']};
  }
}
