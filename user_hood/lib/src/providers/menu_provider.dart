import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:user_hood/src/models/menu_models.dart';
import 'package:user_hood/src/preferences_user/preferences_user.dart';

class MenuProvider {
  Response resp;
  final dio = new Dio();
  final String _url = 'https://dev-hood.herokuapp.com/ws';
  final prefs = PreferenciasUsuario();

  final opts = Options(
      followRedirects: false,
      validateStatus: (status) {
        return status < 500;
      });

  Future<List<Menu>> _procesarRespuesta(String url) async {
    final resp = await dio.get(url, options: opts);
    final decodedData = resp.data;
    final menus = new Menus.fromJson(decodedData);
    // print(menus.msg);
    // print(menus.data[1].nombre);
    return menus.data;
  }

  // Future<List<Menu>> _procesarRespuestaToken(String url) async {
  //   final token = prefs.token;
  //   final optsT = Options(
  //       followRedirects: false,
  //       headers: {"access-token": "Bearer $token"},
  //       validateStatus: (status) {
  //         return status < 500;
  //       });
  //   print(token);
  //   final resp = await dio.get(url, options: optsT);
  //   final decodedData = resp.data;
  //   final menus = new Menus.fromJson(decodedData);
  //   return menus.data;
  // } //////////////////////////procesar respuesta con el token si imprime el token

  Future<List<Menu>> getMenu() async {
    final String url = '$_url/menu/all';
    return await _procesarRespuesta(url);
  }

  Future<List<Menu>> getMenuxUser(String id) async {
    final String url = '$_url/restaurant/$id/menu/all';
    return await _procesarRespuesta(url);
  }

  Future<List<Menu>> getMenuDetalle(String menuId) async {
    final String url = '$_url/menu/$menuId';
    return await _procesarRespuesta(url);
  }

  Future<Map<String, dynamic>> postMenuComment(
      String userCode, int rate, String comment, String postBy) async {
    final String url = '$_url/menu/$userCode/comentario';
    final menuComment = {
      'rating': rate,
      'comment': comment,
      'postedBy': postBy
    };
    resp = await dio.put(url, data: json.encode(menuComment), options: opts);

    Map<String, dynamic> decodedResp = resp.data;
    return {'ok': true, 'token': decodedResp['mensaje']};
  }
}
