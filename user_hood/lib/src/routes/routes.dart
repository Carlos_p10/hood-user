import 'package:flutter/material.dart';
import 'package:user_hood/src/pages/BasePage.dart';
import 'package:user_hood/src/pages/Cards.dart';
import 'package:user_hood/src/pages/ProximamentePage.dart';
import 'package:user_hood/src/pages/Restaurant.dart';
import 'package:user_hood/src/pages/Restaurants.dart';
import 'package:user_hood/src/pages/addCard.dart';
import 'package:user_hood/src/pages/cartPager.dart';
import 'package:user_hood/src/pages/detalle_comida.dart';
import 'package:user_hood/src/pages/login_page.dart';
import 'package:user_hood/src/pages/registro_page.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    '/': (BuildContext context) => BasePage(),
    'restaurants': (BuildContext context) => RestaurantsPage(),
    'login': (BuildContext context) => LoginPage(),
    'registro': (BuildContext context) => RegistroPage(),
    'mycards' : (BuildContext context) => CardsPage(),
    'addCard' : (BuildContext context) => AddCard(),
    'cart': (BuildContext context) => CartPage(),
    'detalleComida': (BuildContext context) => DetalleComida(),
    'restaurant': (BuildContext context) => RestaurantPage(),
    'proxim': (BuildContext context) => Proximamente(),
  };
}
